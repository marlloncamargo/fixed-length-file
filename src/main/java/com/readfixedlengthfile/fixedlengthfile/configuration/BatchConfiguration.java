package com.readfixedlengthfile.fixedlengthfile.configuration;

import com.readfixedlengthfile.fixedlengthfile.model.DataFileInput;
import com.readfixedlengthfile.fixedlengthfile.processor.DataFileItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public FlatFileItemReader<DataFileInput> reader() {
        FixedLengthTokenizer tokenizer = new FixedLengthTokenizer();
        tokenizer.setNames("content1", "content2", "content3");
        tokenizer.setColumns(new Range(1, 6),
                new Range(7, 12),
                new Range(13));

        FlatFileItemReader<DataFileInput> reader = new FlatFileItemReader<>();
        reader.setResource(new ClassPathResource("sample-input-file.txt"));
        reader.setLineMapper(new DefaultLineMapper<DataFileInput>() {{
            setLineTokenizer(tokenizer);
            setFieldSetMapper(new BeanWrapperFieldSetMapper<DataFileInput>() {{
                setTargetType(DataFileInput.class);
            }});
        }});
        return reader;
    }

    @Bean
    public FlatFileItemWriter<String> writer() {
        return new FlatFileItemWriterBuilder<String>()
                .name("inputFileWriter")
                .resource(new FileSystemResource("./output/output-file.txt"))
                .lineAggregator(new PassThroughLineAggregator<>())
                .build();
    }

    @Bean
    public Job sampleJob() {
        return jobBuilderFactory.get("sampleJob")
                //.listener(jobListener)
                //.incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("sampleDataStep")
                .<DataFileInput, String> chunk(10)
                .reader(reader())
                .processor(new DataFileItemProcessor())
                .writer(writer())
                .build();
    }
}
