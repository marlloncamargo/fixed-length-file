package com.readfixedlengthfile.fixedlengthfile.processor;

import com.readfixedlengthfile.fixedlengthfile.model.DataFileInput;
import org.springframework.batch.item.ItemProcessor;

import java.util.Random;

public class DataFileItemProcessor implements ItemProcessor<DataFileInput, String> {

    @Override
    public String process(DataFileInput dataFile) throws Exception {
        dataFile.setId(new Random().nextLong());
        return dataFile.toString();
    }
}
