package com.readfixedlengthfile.fixedlengthfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FixedLengthFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(FixedLengthFileApplication.class, args);
	}

}
