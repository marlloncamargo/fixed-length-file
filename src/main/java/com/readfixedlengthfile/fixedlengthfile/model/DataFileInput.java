package com.readfixedlengthfile.fixedlengthfile.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DataFileInput {

    private Long id;
    private String content1;
    private String content2;
    private String content3;

}
